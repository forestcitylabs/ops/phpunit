FROM registry.gitlab.com/forestcitylabs/ops/php-base:latest

RUN apk add --update php7-xdebug

# Enable xdebug.
COPY config/xdebug.ini /etc/php7/conf.d/xdebug.ini

# Create a small shell script for executing console commands.
RUN curl -L "https://phar.phpunit.de/phpunit.phar" -o /usr/local/bin/phpunit
RUN chmod a+x /usr/local/bin/phpunit
